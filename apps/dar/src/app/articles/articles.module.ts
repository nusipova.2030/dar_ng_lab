import { ArticleResolver } from './article.resolver';
import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticlesComponent } from './articles/articles.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article/article.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ArticlesComponent,
    ArticlesListComponent,
    ArticleComponent
  ],
  imports: [
    CommonModule,
    ArticlesRoutingModule,
    ReactiveFormsModule
  ],
  // providers: [
  //   ArticleResolver
  // ]
})
export class ArticlesModule { }
