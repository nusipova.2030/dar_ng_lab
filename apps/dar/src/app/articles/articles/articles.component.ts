import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {Article, Category} from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { mergeMap, catchError, map, debounceTime } from 'rxjs/operators';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'dar-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {


  articles$: Observable<Article[]>;

  searchTerm: FormControl;



  constructor(
    private httpClient: HttpClient,
    private router: Router
    ) {

   }

  ngOnInit(): void {
    this.searchTerm = new FormControl('');

    this.searchTerm
      .valueChanges
      .pipe (
        debounceTime(400)
      )
      .subscribe( term => {
        console.log(term)
        this.getData()
    })

    this.getData();

  }
  rowClickedHandler(article: Article){
    console.log(article)
    this.router.navigate(['articles', article.id]);
  }
  getData(){
    this.articles$ = this.httpClient
   .get<Article[]>(`/api/articles?limit=10&sort=id:DESC`)
    .pipe(
      catchError(() => of(null)),
      mergeMap(articles => (
        ! articles ? of([]) :
        this.httpClient.get<Category[]>(`/api/categories`)
        .pipe(
          map(categories => {
            return articles.map(article => {
              const category = categories.find( c => c.id === article.category_id);
              return {
                ...article,
                category_title: category ? category.title : 'No category',
              }
            });
          }),
          catchError(() => of(articles))
        )
      )),
      map((articles: Article[] )=> this.searchTerm ?
        articles.filter(a => a.title.toLowerCase().includes(this.searchTerm.value.toLowerCase())) : articles)
    )
  }
}
