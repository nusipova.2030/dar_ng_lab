import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Article } from '@dar-lab-ng/api-interfaces';
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from '@angular/core';


@Injectable()
export class ArticleResolver implements Resolve<Article> {

  constructor(
    private http: HttpClient
  ){}

  resolve(route: ActivatedRouteSnapshot): Observable<Article>{
    const articleId = route.params.id;

    return  this.http
    .get<Article>(`/api/articles/${articleId}`)


  }
}
