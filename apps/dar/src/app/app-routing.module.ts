import { GreetingComponent } from './pages/greeting/greeting.component';
import { NgModule } from "@angular/core";
import { Route, RouterModule}from "@angular/router"



const routes: Route[] = [
    {
        path: 'greetings',
        component: GreetingComponent
    },
    {
        path: 'articles',
        loadChildren: () => import('./articles/articles.module').then(n=> n.ArticlesModule)
    },{
        path: 'categories',
        loadChildren: () =>
          import('./categories/categories.module').then( (m) => m.CategoriesModule),
    },
    {
        path:'**',
        redirectTo: '/'
    }

]

@NgModule({
    imports:  [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule{

}
