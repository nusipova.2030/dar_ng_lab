import { Category } from  '@dar-lab-ng/api-interfaces';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dar-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent {

  @Input()
  categories: Category[] = [];


}
