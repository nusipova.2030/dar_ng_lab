import { catchError } from 'rxjs/operators';
import { Category } from '@dar-lab-ng/api-interfaces';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable()
export class CategoryResolver implements Resolve<Category> {

  constructor(
    private http: HttpClient
  ){}
  resolve(route: ActivatedRouteSnapshot) {
    const categoryId = route.params.id;

    return this.http.get<Category>(`/api/category/${categoryId}`)
      .pipe(catchError(() => {
        return of(null);
      }))

  }

}
