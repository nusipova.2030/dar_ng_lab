import { Category } from '@dar-lab-ng/api-interfaces';
import { CategoriesComponent } from './categories/categories.component';
import { RouterModule, Route } from '@angular/router';
import { NgModule } from '@angular/core';
import { CategoryComponent } from './category/category.component';
import { CategoryResolver } from './category.resolver';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: CategoriesComponent
  },
  {
    path: ':id',
    component: CategoryComponent,
    resolve: {
      category: CategoryResolver
    }
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule{

}
