import { CategoryResolver } from './category.resolver';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories/categories.component'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './category/category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriesListComponent } from './categories-list/categories-list.component';



@NgModule({
  declarations: [
    CategoryComponent,
    CategoriesComponent,
    CategoriesListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CategoriesRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    CategoryResolver
  ]
})
export class CategoriesModule { }
